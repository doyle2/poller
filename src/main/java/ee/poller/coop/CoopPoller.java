package ee.poller.coop;


import static org.springframework.http.HttpMethod.GET;

import java.awt.Toolkit;
import java.util.List;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class CoopPoller implements Runnable {

  @Override
  public void run() {
    try {
      poll();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void poll() throws InterruptedException {
    while (true) {
      ResponseEntity<CoopResource> response = pollDeliveryApi();
      checkForAvailability(response.getBody());
      Thread.sleep(60_000);
    }

  }

  private ResponseEntity<CoopResource> pollDeliveryApi() {
    return new RestTemplate()
        .exchange(
            "https://ecoop.ee/api/v1/time_slots/for_predetermined_period?locationId=4&time_limited=false",
            GET,
            new HttpEntity<String>(new HttpHeaders()),
            CoopResource.class);
  }

  private void checkForAvailability(final CoopResource response) throws InterruptedException {
    if (hasAvailableForTodayAndTomorrow(response)) {
      System.out.println(
          "=================================  COOP JACKPOT =================================");

      while (true) {
        Toolkit.getDefaultToolkit().beep();
        Thread.sleep(3000);
      }
    }
  }

  private boolean hasAvailableForTodayAndTomorrow(CoopResource coopResource) {
    final List<DayResource> todayAndTomorrow = coopResource.getDays().subList(0, 2);
    System.out.println(todayAndTomorrow);
    return todayAndTomorrow.stream().anyMatch(day -> day.getSlots().stream().anyMatch(
        SlotResource::isUsable));
  }
}
