package ee.poller.barbora;


import static org.springframework.http.HttpMethod.GET;

import java.awt.Toolkit;
import java.util.List;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class BarboraPoller implements Runnable {

  private String AUTH_COOKIE = "keyHere";

  @Override
  public void run() {
    try {
      poll();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void poll() throws InterruptedException {
    while (true) {
      ResponseEntity<BarboraResource> response = pollDeliveryApi();
      checkForAvailability(response);
      Thread.sleep(60_000);
    }

  }

  private ResponseEntity<BarboraResource> pollDeliveryApi() {
    HttpHeaders headers = new HttpHeaders();
    headers.add("Cookie", ".BRBAUTH=" + AUTH_COOKIE);
    headers.add("Cookie", "region=barbora.ee");
    headers.add("Authorization", "Basic YXBpa2V5OlNlY3JldEtleQ==");

    try {
      return new RestTemplate()
          .exchange("https://www.barbora.ee/api/eshop/v1/cart/deliveries",
              GET,
              new HttpEntity<String>(headers),
              BarboraResource.class);
    } catch (HttpClientErrorException.NotFound e) {
      throw new RuntimeException("They changed the API", e);
    }
  }

  private void checkForAvailability(ResponseEntity<BarboraResource> response)
      throws InterruptedException {

    if (hasAvailableForTodayAndTomorrow(response.getBody())) {
      System.out.println(
          "=================================  BARBORA JACKPOT =================================");
      while (true) {
        Toolkit.getDefaultToolkit().beep();
        Thread.sleep(3000);
      }
    }
  }

  private boolean hasAvailableForTodayAndTomorrow(BarboraResource barboraResource) {
    final List<MatrixResource> matrix = barboraResource.getDeliveries().get(0).getParams()
        .getMatrix();
    final List<MatrixResource> todayAndTomorrow = matrix.subList(0, 2);
    System.out.println(todayAndTomorrow);
    return todayAndTomorrow.stream().anyMatch(day -> available(day.getHours()));
  }

  private boolean available(List<HourResource> hours) {
    return hours.stream().anyMatch(HourResource::isAvailable);
  }

}
