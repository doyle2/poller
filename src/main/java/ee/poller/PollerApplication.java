package ee.poller;

import ee.poller.barbora.BarboraPoller;
import ee.poller.coop.CoopPoller;

public class PollerApplication {

  public static void main(String[] args) {
    new PollerApplication().runPollers();
  }

  private void runPollers() {
    Thread barboraThread = new Thread(new BarboraPoller());
    Thread coopThread = new Thread(new CoopPoller());

    barboraThread.start();
    coopThread.start();
  }

}
